FROM aslong87/erlang:latest
MAINTAINER Andrew Long <aslong87@gmail.com>

RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN apt-get update && apt-get install -y \
  elixir

RUN mix local.hex --force
RUN mix local.rebar --force
